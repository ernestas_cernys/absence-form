document.addEventListener("DOMContentLoaded", function () {

  /**
   * get url parametres
   */
  function getUrlParams() {
    const queryString = window.location.search
    const urlParams = new URLSearchParams(queryString)
    return urlParams
  }

  /**
   * setting disabled to all inputs of radio type
   */
  function makeRadiosReadOnly() {
    let radios = document.querySelectorAll('input[type=radio]')
    console.log(radios)
    for (let radio of radios) {
      radio.setAttribute('disabled', '')
    }
  }

  /**
   * Filling data to input elements from url parametres
   */
  function fillParamData() {
    const entries = getUrlParams().entries()

    // data for text type input
    for (let entry of entries) {
      let element = document.querySelector('#' + entry[0])

      // data for inexistant element
      if (!element) { continue }

      element.value = entry[1]
    }

    // setting radio from params
    let element = document.querySelector('#' + getUrlParams().get('reason'))
    element.setAttribute('checked', '')

    // full name insert
    let fullName = document.querySelector('#full-name')
    fullName.innerText = ' ' + getUrlParams().get('first-name') + ' ' + getUrlParams().get('last-name')

    makeRadiosReadOnly()

  }

  getData('/assets/json/absenceReason.json').then(data => { fillReasons(data) }).then(() => { fillParamData() })

  let printButton = document.querySelector('#print-button')
  printButton.addEventListener('click', () => {window.print()})

})
