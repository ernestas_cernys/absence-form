
  /* Fetching JSON data */
  function getData(url) {

    return fetch(url)
      .then(response =>{
        return response.json()
      })
      .catch(reject => {
        window.alert(reject)
      })
  }

  /**
   * Generate reason of absence table
   * @param {*} jsonData
   */
  function fillReasons(jsonData) {

    const gridContainer = document.querySelector('dl.box')
    const template = document.querySelector('#reason-template')

    while (gridContainer.firstChild) {
      gridContainer.removeChild(gridContainer.firstChild)
    }

    jsonData.forEach(jsonObject => {
      const clone = template.content.cloneNode(true);
      const dt = clone.querySelector('dt')
      const dd = clone.querySelector('dd')
      const divItem = clone.querySelector('div.item')
      divItem.removeChild(dd)

      dt.innerText = jsonObject.code

      jsonObject.reasons.forEach(listItem => {
        ddClone = dd.cloneNode(true)

        const input = ddClone.querySelector('input')
        const label = ddClone.querySelector('label')

        // input
        input.id = listItem.id
        input.setAttribute('value', listItem.id)

        // label
        label.setAttribute('for', listItem.id)
        label.innerText = listItem.text

        divItem.appendChild(ddClone)
      })

      gridContainer.appendChild(clone)
    });
  }
