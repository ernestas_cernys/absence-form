var liveServer = require("live-server");

var params = {
    port: 8181, // Set the server port. Defaults to 8080.
    open: true, // When false, it won't load your browser by default.
    root: "src/", // Set root directory that's being served. Defaults to cwd.
    mount: [['/node_modules', './node_modules']], // Mount a directory to a route.

};
liveServer.start(params);
