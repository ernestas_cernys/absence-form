
document.addEventListener("DOMContentLoaded", function () {

  getData('/assets/json/absenceReason.json').then(data => {fillReasons(data)} )

  /* Absence duration toggle for single or multiple days */

  const singleDayRadio = document.querySelector("#single")
  const multiDayRadio = document.querySelector("#multiple")
  const singleDayDiv = document.querySelector("#single-day")
  const multipleDaysDiv = document.querySelector("#multiple-days")
  const singleDayInputs = document.querySelectorAll('#single-day input')
  const multipleDaysInputs = document.querySelectorAll('#multiple-days input')

  // toggle required attribute for single multiple days div
  function toggleRequired(required, notRequired) {
    required.forEach(e => e.setAttribute('required', 'true'))
    notRequired.forEach(e => e.removeAttribute('required'))
  }

  // toggle visibility and validation of hidden single multiple days div
  function toggleAbsenceDuration(visible, hidden, required, notRequired) {
    visible.classList.remove("hidden")
    hidden.classList.add("hidden")
    toggleRequired(required, notRequired)
  }

  // Single day by default
  toggleAbsenceDuration(singleDayDiv, multipleDaysDiv, singleDayInputs, multipleDaysInputs)

  // eventListeners
  singleDayRadio.addEventListener('click', function () { toggleAbsenceDuration(singleDayDiv, multipleDaysDiv, singleDayInputs, multipleDaysInputs) })
  multiDayRadio.addEventListener('click', function () { toggleAbsenceDuration(multipleDaysDiv, singleDayDiv, multipleDaysInputs, singleDayInputs) })

  /* end Absence duration toggle for single or multiple days */

  // sigle day time

  const startTime = document.querySelector('#start-time')
  const endTime = document.querySelector('#end-time')

  function getMinTime() {
    const values = startTime.value.split(':')
    let hours = values[0]
    let mins = values[1]
    mins = parseInt(mins) + 1
    if (mins === 60) {
      mins = '00'
      hours = parseInt(hours) + 1
    }
    return hours.toString().padStart(2, '0') + ':' + mins.toString().padStart(2, '0')
  }

  function getMaxTime() {
    const values = endTime.value.split(':')
    let hours = values[0]
    let mins = values[1]
    mins = parseInt(mins) - 1
    if (mins === -1) {
      mins = 0
      hours = parseInt(hours) - 1
    }
    return hours.toString().padStart(2, '0') + ':' + mins.toString().padStart(2, '0')
  }

  startTime.addEventListener('change', function () { endTime.setAttribute('min', getMinTime()) })
  endTime.addEventListener('change', function () { startTime.setAttribute('max', getMaxTime()) })

  // Multiple days management

  const startDate = document.querySelector('#start-date')
  const endDate = document.querySelector('#end-date')
  const totalDays = document.querySelector('#total-days')
  const span = document.querySelector('#total-days-text')

  function GetFormattedDate(date) {
    const month = (date.getMonth() + 1).toString()
    const day = (date.getDate()).toString()
    const year = date.getFullYear()
    return year + "-" + month.padStart(2, '0') + "-" + day.padStart(2, '0');
  }

  function setDateDifference() {
    let days = Math.floor((new Date(endDate.value) - new Date(startDate.value)) / (1000 * 60 * 60 * 24))
    if (!isNaN(days)) {
      totalDays.setAttribute('value', days)
      if (days === 1) {
        span.textContent = "jour complet"
      }
    }
  }

  function minDate() {
    let date = new Date(startDate.value)
    date.setDate(date.getDate() + 1)
    endDate.setAttribute('min', GetFormattedDate(date))
    setDateDifference()
  }

  function maxDate() {
    let date = new Date(endDate.value)
    date.setDate(date.getDate() - 1)
    startDate.setAttribute('max', GetFormattedDate(date))
    setDateDifference()
  }

  startDate.addEventListener('change', minDate)
  endDate.addEventListener('change', maxDate)

  // Submit validation

  const form = document.querySelector('.needs-validation')

  form.addEventListener('submit', function (event) {
    if (form.checkValidity() === false) {
      event.preventDefault()
      event.stopPropagation()
    }
    form.classList.add('was-validated')
  })

  // end Submit validation


})


